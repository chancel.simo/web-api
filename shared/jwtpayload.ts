export interface JwtPayload {
    userName: string;
    id: string;

    iat?: number;
    exp?: number;

    lastName?: string;

}