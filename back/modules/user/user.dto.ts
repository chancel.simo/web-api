import { ApiPropertyOptional } from "@nestjs/swagger";
import { GenericResponse } from "../generic/genericResponse";

export class UserDto {
    @ApiPropertyOptional()
    id?: string;

    @ApiPropertyOptional()
    userName?: string;

    @ApiPropertyOptional()
    password?: string;

    @ApiPropertyOptional({ type: String, format: 'date-time' })
    birthDate?: Date;

    @ApiPropertyOptional()
    lastName?: string;

    @ApiPropertyOptional()
    access_token?: string;
}

export class GetUserResponse extends GenericResponse {
    @ApiPropertyOptional({ type: () => UserDto })
    user?: UserDto;
}

export class GetLoginRequest {
    @ApiPropertyOptional()
    userName?: string;

    @ApiPropertyOptional()
    password?: string;
}